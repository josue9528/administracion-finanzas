﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Data.SqlClient;
using RegistrarUsuarios;

namespace administracion_finanzas
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();

            //mandamos a llamar el splash
            Thread hilo = new Thread(mostrarSplash);
            hilo.Start();
            Thread.Sleep(5000);
            hilo.Abort();
        }
        ClassBD BD = new ClassBD();
        int res = -1;
        

        void mostrarSplash()
        {
            Splash splash = new Splash();
            splash.ShowDialog();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!TxtCorreo.Text.Equals("") && !TxtContrasenia.Text.Equals(""))
            {
                SqlDataReader ResConsult;
                string Consulta = "select * from usuarios where mail_usuario= '" + TxtCorreo.Text + "';";

                //Llama al metodo Buscar que se encuentra en la clase y envia la consulta a ejecutar
                //El resultado de la consulta se asigna a ResConsult, objeto de lectura de datos

                ResConsult = BD.Buscar(Consulta);
                if (ResConsult.Read())
                {
                    //Si se encuentra un registro con las especificaciones proporcionadas entonces
                    //Compara la contraseña
                    if (ResConsult.GetString(4).ToString().Equals(TxtContrasenia.Text))
                    {
                        this.Hide();
                        Registrar_Usuario RegUss = new Registrar_Usuario();
                        RegUss.Show();
                    }
                    else
                    {
                        MessageBox.Show("Contraseña incorrecta", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Usuario no registrado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                ResConsult.Close();
                ClassBD.conn.Close();
            }
            else
            {
                MessageBox.Show("Verifique que campos de texto esten llenos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void BtnRegistrar_Click(object sender, EventArgs e)
        {
            this.Hide();
            Registrar_Usuario RegUss = new Registrar_Usuario();
            RegUss.Show();
        }
    }
}
