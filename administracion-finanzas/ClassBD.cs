﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace administracion_finanzas
{
    class ClassBD
    {
        //Se define la conexion a base de datos 

        //INSTANCIAS
        //Dani Casillas
        //public static SqlConnection conn = new SqlConnection("Data Source=DESKTOP-1162JAA\\JDSQLSERVER; Initial Catalog=admin_finanzas; User ID=sa; Password=123456;");

        //Cesar Garcia
        public static SqlConnection conn = new SqlConnection
           ("Initial Catalog= admin_finanzas; Data Source=localhost;Integrated Security=SSPI;");


        public DataSet ConsultaTab(string tabla, String campo)
        {
            //se genera un objeto de la clase DataSet (almacen de datos, representa una base de datos en 
            //memoria y desconectada del proveedor de datos, contiene tablas y sus relaciones )
            DataSet datSet = new DataSet();

            //se genera un objeto Adaptador (adaptador entre un objeto DataSet y sus operaciones 
            // fisicas en la base de datos)(select, insert, update y delete)
            SqlDataAdapter adaptador = new SqlDataAdapter();

            //se crea la cadena que contiene la consulta en lenguaje SQL
            string consulta = "Select * From " + tabla + " order by " + campo + " ASC";

            // se genera un objeto Command que nos va a permitir ejecutar la sentencia SQL sobre la 
            //fuente de datos a la que estamos accediendo
            SqlCommand comando = new SqlCommand(consulta, conn);

            //se asigna al adaptador el comando a ejecutar
            adaptador.SelectCommand = comando;

            // se abre la conexión a la base de datos
            conn.Open();


            //se obtienen los datos de la consulta ejecutada
            adaptador.Fill(datSet, tabla);

            //se cierra la conexión de la BD
            conn.Close();

            //retorna los datos obtenidos
            return datSet;
        }
        public SqlDataReader Buscar(string CodConsulta)
        {
            //Se genera un objeto Command que nos va a permitir ejecutar la sentencia sql sobre
            //la fuente de datos a la que estamo accediendo (Conexion)
            SqlCommand comando = new SqlCommand(CodConsulta, conn);

            conn.Open();

            //Crear el objeto lector de datos
            SqlDataReader lector = comando.ExecuteReader();

            //Regreso los objetos Leidos
            return lector;
        }
        public int ABM(String instruccion)//Alta Baja Modificar
        {
            int respuesta = 1;

            //asigna al comando la instruccion SQL y la conexion a utilizar
            SqlCommand comando = new SqlCommand(instruccion, conn);

            //Abre la conexion a la base de datos 
            conn.Open();

            //ejecuta instruccion sql y esta devuelve valor, 1 = exito, o error
            respuesta = comando.ExecuteNonQuery();

            conn.Close();
            return respuesta;
        }
        //Este buscar es para el comboBox
        /*
         public DataSet BuscarCliente(string consu, string tabla)
        {
            DataSet ConjuntosDatos = new DataSet();
            SqlCommand comando = new SqlCommand(consu, conn);
            SqlDataAdapter adaptador = new SqlDataAdapter();
            adaptador.SelectCommand = comando;
            conn.Open();
            adaptador.Fill(ConjuntosDatos, tabla);
            conn.Close();
            return ConjuntosDatos;
            
        }
         */
    }
}
