﻿namespace administracion_finanzas
{
    partial class FormGastos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormGastos));
            this.DgTablaGastos = new System.Windows.Forms.DataGridView();
            this.BtnMenu = new System.Windows.Forms.Button();
            this.BtnLogout = new System.Windows.Forms.Button();
            this.lbTitulo = new System.Windows.Forms.Label();
            this.TxtBuscar = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TxbNombre = new System.Windows.Forms.TextBox();
            this.TxbMonto = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.DattimPago = new System.Windows.Forms.DateTimePicker();
            this.BtnAgrear = new System.Windows.Forms.Button();
            this.BtnModificar = new System.Windows.Forms.Button();
            this.BtnBuscar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DgTablaGastos)).BeginInit();
            this.SuspendLayout();
            // 
            // DgTablaGastos
            // 
            this.DgTablaGastos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgTablaGastos.Location = new System.Drawing.Point(25, 86);
            this.DgTablaGastos.Name = "DgTablaGastos";
            this.DgTablaGastos.Size = new System.Drawing.Size(780, 210);
            this.DgTablaGastos.TabIndex = 0;
            // 
            // BtnMenu
            // 
            this.BtnMenu.Location = new System.Drawing.Point(652, 13);
            this.BtnMenu.Name = "BtnMenu";
            this.BtnMenu.Size = new System.Drawing.Size(67, 51);
            this.BtnMenu.TabIndex = 1;
            this.BtnMenu.Text = "Menu";
            this.BtnMenu.UseVisualStyleBackColor = true;
            this.BtnMenu.Click += new System.EventHandler(this.BtnMenu_Click);
            // 
            // BtnLogout
            // 
            this.BtnLogout.Location = new System.Drawing.Point(738, 12);
            this.BtnLogout.Name = "BtnLogout";
            this.BtnLogout.Size = new System.Drawing.Size(67, 51);
            this.BtnLogout.TabIndex = 2;
            this.BtnLogout.Text = "Cerrar Sesion";
            this.BtnLogout.UseVisualStyleBackColor = true;
            // 
            // lbTitulo
            // 
            this.lbTitulo.AutoSize = true;
            this.lbTitulo.BackColor = System.Drawing.Color.Transparent;
            this.lbTitulo.Font = new System.Drawing.Font("Constantia", 24.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTitulo.Location = new System.Drawing.Point(19, 24);
            this.lbTitulo.Name = "lbTitulo";
            this.lbTitulo.Size = new System.Drawing.Size(294, 40);
            this.lbTitulo.TabIndex = 3;
            this.lbTitulo.Text = "Control de Gastos";
            // 
            // TxtBuscar
            // 
            this.TxtBuscar.Location = new System.Drawing.Point(518, 318);
            this.TxtBuscar.Name = "TxtBuscar";
            this.TxtBuscar.Size = new System.Drawing.Size(201, 20);
            this.TxtBuscar.TabIndex = 5;
            this.TxtBuscar.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(76, 354);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Nombre";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(449, 321);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Buscar";
            // 
            // TxbNombre
            // 
            this.TxbNombre.Location = new System.Drawing.Point(138, 351);
            this.TxbNombre.Name = "TxbNombre";
            this.TxbNombre.Size = new System.Drawing.Size(200, 20);
            this.TxbNombre.TabIndex = 8;
            // 
            // TxbMonto
            // 
            this.TxbMonto.Location = new System.Drawing.Point(138, 378);
            this.TxbMonto.Name = "TxbMonto";
            this.TxbMonto.Size = new System.Drawing.Size(200, 20);
            this.TxbMonto.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 381);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Monto Aproximado";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 414);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Fecha de Pago";
            // 
            // DattimPago
            // 
            this.DattimPago.Location = new System.Drawing.Point(138, 408);
            this.DattimPago.Name = "DattimPago";
            this.DattimPago.Size = new System.Drawing.Size(200, 20);
            this.DattimPago.TabIndex = 13;
            // 
            // BtnAgrear
            // 
            this.BtnAgrear.Location = new System.Drawing.Point(538, 378);
            this.BtnAgrear.Name = "BtnAgrear";
            this.BtnAgrear.Size = new System.Drawing.Size(67, 51);
            this.BtnAgrear.TabIndex = 14;
            this.BtnAgrear.Text = "Menu";
            this.BtnAgrear.UseVisualStyleBackColor = true;
            // 
            // BtnModificar
            // 
            this.BtnModificar.Location = new System.Drawing.Point(669, 378);
            this.BtnModificar.Name = "BtnModificar";
            this.BtnModificar.Size = new System.Drawing.Size(67, 51);
            this.BtnModificar.TabIndex = 15;
            this.BtnModificar.Text = "Menu";
            this.BtnModificar.UseVisualStyleBackColor = true;
            // 
            // BtnBuscar
            // 
            this.BtnBuscar.Location = new System.Drawing.Point(738, 302);
            this.BtnBuscar.Name = "BtnBuscar";
            this.BtnBuscar.Size = new System.Drawing.Size(67, 51);
            this.BtnBuscar.TabIndex = 16;
            this.BtnBuscar.Text = "Menu";
            this.BtnBuscar.UseVisualStyleBackColor = true;
            // 
            // FormGastos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 461);
            this.Controls.Add(this.BtnBuscar);
            this.Controls.Add(this.BtnModificar);
            this.Controls.Add(this.BtnAgrear);
            this.Controls.Add(this.DattimPago);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TxbMonto);
            this.Controls.Add(this.TxbNombre);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TxtBuscar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbTitulo);
            this.Controls.Add(this.BtnLogout);
            this.Controls.Add(this.BtnMenu);
            this.Controls.Add(this.DgTablaGastos);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormGastos";
            this.Text = "Gastos";
            ((System.ComponentModel.ISupportInitialize)(this.DgTablaGastos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DgTablaGastos;
        private System.Windows.Forms.Button BtnMenu;
        private System.Windows.Forms.Button BtnLogout;
        private System.Windows.Forms.Label lbTitulo;
        private System.Windows.Forms.TextBox TxtBuscar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxbNombre;
        private System.Windows.Forms.TextBox TxbMonto;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker DattimPago;
        private System.Windows.Forms.Button BtnAgrear;
        private System.Windows.Forms.Button BtnModificar;
        private System.Windows.Forms.Button BtnBuscar;
    }
}